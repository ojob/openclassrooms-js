
var figure_map = {
    // table de conversion des nombres usuels
    // 70 et 90 figurent ici, même s'il y aurait sans doute
    // moyen de s'en passer
    0: 'zéro',
    1: 'un',
    2: 'deux',
    3: 'trois',
    4: 'quatre',
    5: 'cinq',
    6: 'six',
    7: 'sept',
    8: 'huit',
    9: 'neuf',
    10: 'dix',
    11: 'onze',
    12: 'douze',
    13: 'treize',
    14: 'quatorze',
    15: 'quinze',
    16: 'seize',
    20: 'vingt',
    30: 'trente',
    40: 'quarante',
    50: 'cinquante',
    60: 'soixante',
    70: 'soixante-dix',
    80: 'quatre-vingt',
    90: 'quatre-vingt-dix',
    100: 'cent',
}

function action_convert() {
    // gestion des entrées utilisateur
    var nombre = document.getElementById('nombre').value;
    var output = document.getElementById('resultat');
    // vérification du nombre
    nombre = parseInt(nombre, 10);  // forçons la base 10
    if (isNaN(nombre)) {
        console.log("Entrée NaN : " + nombre);
        output.textContent = 'entrée invalide';
        output.style.color = 'red';
    } else {
        console.log("Nombre demandé : " + nombre);
        result = convertir(nombre);
        output.textContent = result.text;
        output.style.color = result.valid ? 'green' : 'red';
    }
}

function convertir(nombre) {
    // Convertit un nombre (entier) en toutes lettres
    var text_stack = [];
    var result = {'text': "", 'valid': true};
    var trial;
    // si le nombre est dans la table, on le retourne de suite
    if ((trial = figure_map[nombre]) !== undefined) {
        result.text = trial;
        return result;
    }

    // sinon, il faut réfléchir un peu plus...
    var centaines, dizaines, unités;

    unités = nombre % 10;
    dizaines = (nombre - unités) % 100 / 10;
    centaines = (nombre - (unités + dizaines * 10)) / 100;

    if (centaines) {
        if (centaines >= 10) {
            text_stack.push("X");
            valid = false;
        } else if (centaines == 1) {
            text_stack.push(figure_map[100]);
        } else if ((trial = figure_map[centaines]) !== undefined) {
            text_stack.push(trial + "-cent");
        } else {
            console.log("trop de centaines !")
            text_stack.push("X");
            result.valid = false;
        }
    }
    if ((trial = figure_map[unités + 10 * dizaines]) !== undefined) {
        text_stack.push(trial);
    } else {
        // on a forcément des dizaines, sinon on aurait eu un
        // résultat à la requête à figure_map
        if (unités > 0 & unités <= 6 & (dizaines == 7 | dizaines == 9)) {
            // attention, cas tordu : il faut compter les dizaines
            // comme si il y en avait une de moins, et incrémenter
            // les unités pour exploiter les 'onze', 'douze', etc.
            dizaines--;
            unités += 10;
        }
        // on extrait les dizaines
        if ((trial = figure_map[10 * dizaines]) !== undefined) {
            // cas particulier : pour 80 seul (sans unités), il
            // faut ajouter un 's'
            if (dizaines == 8 & unités == 0) {
                trial += 's';
            }
            text_stack.push(trial);
        }
        if (unités == 1 & dizaines <= 6) {
            // autre cas particulier : on ajoute '-et-' avant
            // 'un' pour 21, 31, 41, 51, 61
            text_stack.push('et');
        }
        // et on extrait maintenant normalement les textes
        if (unités > 0) {
            text_stack.push(figure_map[unités]);
        }
    }

    result.text = text_stack.join("-");
    console.log("résultat de conversion : " + result.text);
    return result;
}
