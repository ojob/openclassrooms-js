var nodes_creator = {
    'text': (text) => {
        return document.createTextNode(text)
    },
    'other': (tag, text) => {
        var node = document.createElement(tag);
        var text_node = document.createTextNode(text);
        node.appendChild(text_node);
        return node;
    },
}

var à_créer = [
  ['text', "Le ", {}],
  ['strong', "World Wide Web Consortium", {}],
  ['text', ", abrégé par le sigle ", {}],
  ['strong', "W3C", {}],
  ['text', ", est un ", {}],
  ['a', "organisme de standardisation",
   {'href': 'http://fr.wikipedia.org/wiki/Organisme_de_normalisation',
    'title': "Organisme de normalisation"}
  ],
  ['text', " à but non-lucratif chargé de promouvoir la compatibilité des technologies du ", {}],
  ['a', "World Wide Web",
   {'href': 'http://fr.wikipedia.org/wiki/World_Wide_Web',
    'title': "World Wide Web"}],
  ['text', ".", {}],
  ]

function creer_contenu() {
    console.log("click");
    for (var i = 0, nb = à_créer.length; i < nb; i++) {
        var child_type = à_créer[i][0];
        var child_content = à_créer[i][1];
        if (child_type == 'text') {
            var child = nodes_creator[child_type](child_content);
        } else {
            var child = nodes_creator['other'](child_type, child_content);
        }
        for (var attr_id in à_créer[i][2]) {
          child.setAttribute(attr_id, à_créer[i][2][attr_id]);
        }
        document.getElementById('divTP1').appendChild(child);
    }
}
