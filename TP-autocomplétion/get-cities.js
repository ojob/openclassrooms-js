(function IIEF() {

const HOST = 'http://localhost';
const PORT = '5000';
const CITIES_ENTRYPOINT = '/cities/';
const TIMEOUT = 900;  // milliseconds

var searchElem = document.getElementById('city');
var searchStatus = document.getElementById('searchStatus');
var resultElem = document.getElementById('results');
var curSearch, curXHR;
var selectedResult = -1;

function showMatchingCities() {
    if (typeof curXHR !== 'undefined' && curXHR.readyState < XMLHttpRequest.DONE) {
        // interrupt current xhr, to avoid its bound events to fire unexpectingly
        curXHR.abort();
    }
    var cityPart = searchElem.value.trim();
    if (cityPart == curSearch) {
        // same search, do not fire a new search
        return
    }
    curSearch = cityPart;
    if (curSearch.length === 0) {
        showSearchStatusFactory('no search')();
    } else {
        showSearchStatusFactory('loading...')();
        curXHR = buildSearchXHR(curSearch);
        bindXHR(curXHR);
        curXHR.send(null);
    }
}
function showSearchStatusFactory(status) {
    return function() {
        searchStatus.innerText = status;
    }
}
function buildSearchXHR(cityPart) {
    var xhr = new XMLHttpRequest();
    var apiUrl = HOST + ':' + PORT + CITIES_ENTRYPOINT + encodeURIComponent(cityPart);
    xhr.open('GET', apiUrl);
    xhr.timeout = TIMEOUT;
    return xhr;
}
function bindXHR(xhr) {
    xhr.addEventListener('timeout', showSearchStatusFactory('(timeout)'));
    xhr.addEventListener('readystatechange', xhrUpdateHandler(xhr));
}

function isXHRCompleted(xhr) {
    return xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200;
}
function xhrUpdateHandler(xhr) {
    return function() {
        if (isXHRCompleted(xhr)) {
            var response = xhr.response;
            if (response.length > 0) {
                showSearchStatusFactory('')();
                response = xhr.response.split('|');
            } else {
                showSearchStatusFactory('(no match)')();
            }
            displayResults(response);
        }
    }
}
function displayResults(results = '') {
    clearResults();
    selectedResult = -1;
    for(var i = 0; i < results.length; i++) {
        appendSearchResult(resultElem, results[i]);
    }
}
function clearResults() {
    resultElem.innerText = '';
}
function appendSearchResult(resultElem, resultItem) {
    var div = resultElem.appendChild(document.createElement('div'));
    div.innerHTML = resultItem;
    div.addEventListener('click', chooseResultFactory(div));
}
function chooseResultFactory(elem) {
    return function() {
        searchElem.value = elem.innerHTML;
        displayResults();
    }
}

function keyupCallbackFactory() {
    var upKeyCode = 38, downKeyCode = 40, enterKeyCode = 13;
    return function(event) {
        if (event.keyCode == upKeyCode) {
            selectPreviousResult();
        } else if (event.keyCode == downKeyCode) {
            selectNextResult();
        } else if (event.keyCode == enterKeyCode) {
            enterSelectedResult();
        } else {
            showMatchingCities();
        }
    }
}
function selectPreviousResult() {
    selectResultByDelta(-1);
}
function selectNextResult() {
    selectResultByDelta(+1);
}
function selectResultByDelta(delta) {
    var resultElemList = getResultElemList();
    if (isIndexInList(selectedResult, resultElemList)) {
        resultElemList[selectedResult].className = '';
    }
    selectedResult = (selectedResult + delta) % resultElemList.length;
    console.log("selectedResult: " + selectedResult);
    if (isIndexInList(selectedResult, resultElemList)) {
        resultElemList[selectedResult].className = 'selected';
    }
}
function getResultElemList() {
    return document.getElementById('results').getElementsByTagName('div');
}
function isIndexInList(index, someList) {
    return index >= 0 && index < someList.length;
}
function enterSelectedResult() {
    var resultElemList = getResultElemList();
    chooseResultFactory(resultElemList[selectedResult])();
}

searchElem.addEventListener('keyup', keyupCallbackFactory());

})();