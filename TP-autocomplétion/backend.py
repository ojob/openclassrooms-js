import random
import time
import typing
from pathlib import Path

from flask import Flask, send_from_directory

CITY_DB_PATH = Path('cities.txt')
SEP = '|'


def load(file_path: Path) -> typing.List[str]:
    result: typing.List[str] = []
    with file_path.open() as fh_in:
        for line in fh_in:
            if line.startswith('#') or line == '\n':
                continue
            result.append(line.strip())
    return result


def yield_seq_items_containing_part(seq: typing.Sequence[str], part: str) -> typing.Iterator[str]:
    part = part.lower()
    for item in seq:
        if part in item.lower():
            yield item


def create_app():
    app = Flask(__name__)
    cities = load(CITY_DB_PATH)
    sep = SEP

    @app.route('/')
    @app.route('/<fn>')
    def serve_file(fn: str = None) -> str:
        if fn is None:
            fn = 'get-cities.html'
        return send_from_directory('.', fn)

    @app.route('/cities')
    def get_cities() -> str:
        return sep.join(cities)

    @app.route('/cities/<city_part>')
    def get_matching_city(city_part: str) -> str:
        fake_delay = random.random()
        print(f"fake delay: {fake_delay:.3f}s")
        time.sleep(fake_delay)
        return sep.join(yield_seq_items_containing_part(seq=cities, part=city_part))

    return app


app = create_app()
